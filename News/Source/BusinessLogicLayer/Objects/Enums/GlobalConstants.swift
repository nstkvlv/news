import  UIKit

enum GlobalConstants {
    static let newsCellHeight: CGFloat = 170
    static var isSmallDevice: Bool {
        return UIScreen.main.bounds.height < 670
    }
    static var archivatedNewsViewHeight: CGFloat {
        var height: CGFloat = 500
        if GlobalConstants.isSmallDevice {
            height -= 50
        }
        return height
    }
}
