struct SavedNewsStruct: Codable {
    let title: String
    let author: [String]
    let content: String
    let imageUrl: String?
    let url: String
}
