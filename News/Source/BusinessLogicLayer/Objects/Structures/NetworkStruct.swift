struct News: Decodable {
    struct Response: Decodable {
        struct Result: Decodable {
            struct Fields: Decodable {
                let body: String
                let thumbnail: String?
            }
            struct Author: Decodable {
                let webTitle: String
            }
            let webTitle: String
            let webUrl: String
            let fields: Fields
            let tags: [Author]?
        }
        let results: [Result]
    }
    let response: Response
}
