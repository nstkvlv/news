import UIKit

struct NewsStruct {
    let author: [String]
    let title: String
    let content: String
    var image: UIImage?
    let imageUrl: String?
    let url: String
}
