import UIKit

struct UserInfo: Codable {
    var email: String
    var name: String
    var userID: URL?
    var lastChangesDate: String?
}
