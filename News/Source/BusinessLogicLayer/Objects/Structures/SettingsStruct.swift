struct Settings: Codable {
    var isFaceIdAvailable: Bool
    var isPushNotificationsAvailable: Bool
    var saveToFirebase: Bool
}
