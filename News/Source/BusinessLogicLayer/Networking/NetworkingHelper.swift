import UIKit

final class NetworkingHelper {
    
    func createUrl(host: String, path: String, q: [URLQueryItem]) -> URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = host
        urlComponents.path = path
        urlComponents.queryItems = q
        
        if (urlComponents.url == nil) {
            return URL(string: "")
        }
        guard let url = urlComponents.url else { return URL(string: "")}
        return URL(string: url.absoluteString)
    }
    
    func getNews(from page: Int, with completion: @escaping ([NewsStruct]) -> ()) {
        guard let url = createUrl(
            host: "content.guardianapis.com",
            path: "/search",
            q: [URLQueryItem(name: "api-key", value: Tokens.newsApi),
               URLQueryItem(name: "show-fields", value: "body,thumbnail"),
               URLQueryItem(name: "show-tags", value: "contributor"),
               URLQueryItem(name: "page", value: "\(page)"),
               URLQueryItem(name: "page-size", value: "7")
            ]
        ) else { return }
        let task = URLSession.shared.dataTask(with: url) { data, responce, error in
            if let data = data {
                do {
                    let resultNews = try JSONDecoder().decode(News.self, from: data)
                    var result = [NewsStruct]()
                    for article in resultNews.response.results {
                        var authors = [String]()
                        if let tags = article.tags {
                            for author in tags {
                                authors.append(author.webTitle)
                            }
                        }
                        let title = article.webTitle
                        let content = article.fields.body
                        let imageUrl = article.fields.thumbnail ?? ""
                        let image = self.getImage(from: imageUrl)
                        let url = article.webUrl
                        result.append(NewsStruct(author: authors, title: title, content: content, image: image, imageUrl: imageUrl, url: url))
                    }
                    completion(result)
                } catch {
                    print(error)
                    }
                }
            }
        task.resume()
    }
    
    func getImage(from url: String) -> UIImage? {
        if !url.isEmpty {
            guard let url = URL(string: url),
                  let data = try? Data(contentsOf: url) else { return nil }
            return UIImage(data: data)
        }
        return nil
    }
}
