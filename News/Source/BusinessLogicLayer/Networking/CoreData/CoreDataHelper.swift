import CoreData
import UIKit

private enum Keys {
    static let entityUser = "User"
    static let entitySavedNews = "SavedNews"
    static let email = "email"
    static let name = "name"
    static let password = "password"
    static let lastChangesDate = "lastChangesDate"
    static let savedNews = "savedNews"
    static let news = "news"
    static let settings = "settings"
}

final class CoreDataHelper {
    
    private let hashMaker = HashMaker()
    private let userData = UserDataService.shared
    private let appDelegate = AppDelegate.shared

            
    func registration(email: String, password: String, name: String, with completion: @escaping () -> () = { }) {
        let managedContext = appDelegate.managedContext
        guard let entity = NSEntityDescription.entity(forEntityName: Keys.entityUser, in: managedContext) else { return }
        let user = NSManagedObject(entity: entity, insertInto: managedContext)
        user.setValue(email, forKeyPath: Keys.email)
        let hashPassword = hashMaker.MD5(string: password)
        let hashPasswordString = String(decoding: hashPassword, as: UTF8.self)
        user.setValue(hashPasswordString, forKeyPath: Keys.password)
        user.setValue(name, forKeyPath: Keys.name)
        
        do {
            try managedContext.save()
            userData.userInfo = UserInfo(email: email,
                                         name: name,
                                         userID: user.objectID.uriRepresentation(),
                                         lastChangesDate: userData.userInfo?.lastChangesDate)
            completion()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func logIn(email: String, password: String, with completion: @escaping (_ error: String?, _ array: [NewsStruct]?) -> ()) {
        let managedContext = appDelegate.managedContext
        let hashPassword = hashMaker.MD5(string: password)
        let hashPasswordString = String(decoding: hashPassword, as: UTF8.self)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Keys.entityUser)
        request.predicate = NSPredicate(format: "(email == %@) AND (password == %@)", email, hashPasswordString)
        
         do {
            let result = try managedContext.fetch(request) as? [User]
            if let isContains = result?.indices.contains(0), isContains {
                let user = result?.first
                guard let userIDUrl = user?.objectID.uriRepresentation() else { return }
                fetchData(for: userIDUrl)
                completion(nil, nil)

                DispatchQueue.global(qos: .background).async {
                    FirebaseHelper().logIn(email: email, password: password) { error in
                        guard let error = error else {
                            print("Succeeded")
                            return
                        }
                        print(error)
                    }
                }
            } else {
                FirebaseHelper().logIn(email: email, password: password) { error in
                    if let error = error {
                        completion(error, nil)
                        return
                    }
                    FirebaseHelper().fetchDataFromFirebase(with: {
                        guard let name = self.userData.userInfo?.name else { return }
                        CoreDataHelper().registration(email: email,
                                                      password: password,
                                                      name: name)
                    })
                    FirebaseHelper().fetchSavedNews { array in
                        completion(nil, array)
                    }
                }
            }
        } catch {
            print(" fetch err: ", error)
        }
    }

    private func fetchData(for userIDUrl: URL) {
        let managedContext = appDelegate.managedContext
        guard let userID = managedContext.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: userIDUrl) else { return }
        let object = managedContext.object(with: userID)
        let keys = Array(object.entity.attributesByName.keys)
        let userInfoDictionary = object.dictionaryWithValues(forKeys: keys)
        guard let email = userInfoDictionary[Keys.email] as? String,
              let name = userInfoDictionary[Keys.name] as? String
        else { return }
            
        userData.userInfo = UserInfo(email: email,
                                     name: name,
                                     userID: userIDUrl,
                                     lastChangesDate: userInfoDictionary[Keys.lastChangesDate] as? String)
        guard let settingsData = userInfoDictionary[Keys.settings] as? Data else { return }
        let settings = try? PropertyListDecoder().decode(Settings.self, from: settingsData)
        userData.settings = settings
    }
    
    func deleteCurrentUser(with completion: @escaping () -> ()) {
        let managedContext = appDelegate.managedContext
        guard let userIdUrl = userData.userInfo?.userID,
              let userId = managedContext.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: userIdUrl)
        else { return }
        let object = managedContext.object(with: userId)
        managedContext.delete(object)
        do {
            try managedContext.save()
            completion()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
        
    func changeInfo(to data: String, type: ChangeDataType) {
        let managedContext = appDelegate.managedContext
        if let userIdUrl = userData.userInfo?.userID,
           let userID = managedContext.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: userIdUrl) {
            let object = managedContext.object(with: userID)
            switch type {
                case .password:
                    let hashPassword = hashMaker.MD5(string: data)
                    let hashPasswordString = String(decoding: hashPassword, as: UTF8.self)
                    object.setValue(hashPasswordString, forKeyPath: Keys.password)
                case .name:
                    object.setValue(data, forKeyPath: Keys.name)
                case .lastChangesDate:
                    object.setValue(data, forKeyPath: Keys.lastChangesDate)
            }
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    func addSettings() {
        let managedContext = appDelegate.managedContext
        guard let userIdUrl = userData.userInfo?.userID,
              let userID = managedContext.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: userIdUrl)
        else { return }
        let object = managedContext.object(with: userID)
        let settingsData = try? PropertyListEncoder().encode(userData.settings)
        object.setValue(settingsData, forKey: Keys.settings)
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func saveNews(saveArray: [SavedNewsStruct]) {
        let managedContext = appDelegate.managedContext
        guard let entity = NSEntityDescription.entity(forEntityName: Keys.entitySavedNews, in: managedContext) else { return }
        let news = NSManagedObject(entity: entity, insertInto: managedContext)
        let saveArrayData = try? PropertyListEncoder().encode(saveArray)
        news.setValue(saveArrayData, forKeyPath: Keys.news)
        
        guard let userIdUrl = userData.userInfo?.userID,
              let userID = managedContext.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: userIdUrl) else { return }

        let user = managedContext.object(with: userID)
        user.setValue(news, forKey: Keys.savedNews)

        do {
            try user.managedObjectContext?.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func getSavedNews() -> [NewsStruct]? {
        let managedContext = appDelegate.managedContext
        var newsArray = [NewsStruct]()
        guard let userIdUrl = userData.userInfo?.userID,
              let userID = managedContext.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: userIdUrl)
        else { return nil }
        let user = managedContext.object(with: userID)
        let newsID = user.objectIDs(forRelationshipNamed: Keys.savedNews)
        if let firtsObject = newsID.first {
            let object = managedContext.object(with: firtsObject)
            let keys = Array(object.entity.attributesByName.keys)
            let userInfoDictionary = object.dictionaryWithValues(forKeys: keys)
            guard let savedNews = userInfoDictionary[Keys.news],
                  let savedNewsBin = savedNews as? Data,
                  let fetchedArray = try? PropertyListDecoder().decode([SavedNewsStruct].self, from: savedNewsBin)
            else { return nil }
            fetchedArray.forEach { news in
                newsArray.append(NewsStruct(author: news.author,
                                            title: news.title,
                                            content: news.content,
                                            image: nil,
                                            imageUrl: news.imageUrl,
                                            url: news.url))
            }
            return newsArray
        }
        return nil
    }
    
    func getUserPasword(for userIDUrl: URL) -> String? {
        let managedContext = appDelegate.managedContext
        guard let userID = managedContext.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: userIDUrl) else { return nil }
        let object = managedContext.object(with: userID)
        let keys = Array(object.entity.attributesByName.keys)
        let userInfoDictionary = object.dictionaryWithValues(forKeys: keys)
        guard let password = userInfoDictionary["password"] as? String else { return nil }

        return password
    }
}

      
