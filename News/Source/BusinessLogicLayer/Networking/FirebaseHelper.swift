import Firebase
import UIKit

private enum Keys {
    static let childUser = "user"
    static let name = "name"
    static let lastChangesDate = "lastChangesDate"
    static let settings = "settings"
    static let childNewsArray = "newsArray"
    static let settingsInfo = (faceId: "isFaceIdAvailable",
                               pushNotifications: "isPushNotificationsAvailable",
                               firebase: "saveToFirebase")
    static let news = (title: "title", author: "author", content: "content",
                       imageUrl: "imageUrl", url: "url")
}

final class FirebaseHelper {
    
    private let userData = UserDataService.shared
    private var reference: DatabaseReference?
    private var userId: String
    
    init() {
        reference = Database.database().reference()
        userId = Auth.auth().currentUser?.uid ?? ""
    }
    
    func logIn(email: String, password: String, with completion: @escaping (_ error: String?) -> ()) {
        let hashPassword = HashMaker().MD5(string: password)
        let hashPasswordString = String(decoding: hashPassword, as: UTF8.self)
        Auth.auth().signIn(withEmail: email, password: hashPasswordString) { authResult, error in
            if let _ = authResult,
               let user = Auth.auth().currentUser {
                self.userId = user.uid
                completion(nil)
            }
            guard let error = error else { return }
            completion(error.localizedDescription)
        }
    }
    
    func registerToFirebase(email: String, password: String, with completion: @escaping (_ error: String?) -> ()) {
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            if let _ = authResult,
               let user = Auth.auth().currentUser {
                self.userId = user.uid
                self.addInfoToUser()
                completion(nil)
            }
            
            guard let error = error else { return }
            if (error as NSError).code == 17007 {
                self.addInfoToUser()
                completion(nil)
            } else {
                completion(error.localizedDescription)
            }
        }
    }
    
    func addInfoToUser() {
        guard let name = userData.userInfo?.name,
              let settings = userData.settings
        else { return }
        let settingsDictionary = [
            Keys.settingsInfo.faceId: settings.isFaceIdAvailable,
            Keys.settingsInfo.pushNotifications: settings.isPushNotificationsAvailable,
            Keys.settingsInfo.firebase: settings.saveToFirebase
        ]
        reference?.child(Keys.childUser).child(userId).setValue([
            Keys.name: name,
            Keys.lastChangesDate: userData.userInfo?.lastChangesDate ?? "",
            Keys.settings: settingsDictionary
        ])
    }
    
    func addSavedNews(saveArray: [SavedNewsStruct]) {
        var dictArr = [[String: Any]]()
        saveArray.forEach { news in
            let dict: [String: Any] = [Keys.news.title: news.title,
                                        Keys.news.author: news.author,
                                        Keys.news.content: news.content,
                                        Keys.news.imageUrl: news.imageUrl ?? "",
                                        Keys.news.url: news.url]
            dictArr.append(dict)
        }
        reference?.child(Keys.childUser).child(userId).child(Keys.childNewsArray).setValue(dictArr)
    }
    
    func fetchSavedNews(with completion: @escaping (_ array: [NewsStruct]?) -> ()) {
        var array = [NewsStruct]()
        reference?.child(Keys.childUser).child(userId).child(Keys.childNewsArray).observeSingleEvent(of: .value, with: { dataSnapshot in
            guard let savedNewsArr = dataSnapshot.value as? NSArray else { return }
            
            savedNewsArr.forEach { element in
                guard let newsDict = element as? NSDictionary,
                      let author = newsDict[Keys.news.author] as? [String],
                      let title = newsDict[Keys.news.title] as? String,
                      let content = newsDict[Keys.news.content] as? String,
                      let url = newsDict[Keys.news.url] as? String
                else { return }
                let news = NewsStruct(author: author,
                                      title: title,
                                      content: content,
                                      image: nil,
                                      imageUrl: newsDict[Keys.news.imageUrl] as? String,
                                      url: url)
                array.append(news)
            }
            completion(array)
        })
    }
    
    func fetchDataFromFirebase(with completion: @escaping () -> ()) {
        reference?.child(Keys.childUser).child(userId).observeSingleEvent(of: .value, with: { dataSnapshot in
            let user = Auth.auth().currentUser
            guard let value = dataSnapshot.value as? NSDictionary,
                  let email = user?.email,
                  let name = value[Keys.name] as? String
            else { return }
            self.userData.userInfo = UserInfo(email: email,
                                              name: name,
                                              userID: nil,
                                              lastChangesDate: value[Keys.lastChangesDate] as? String)
            
            guard let settingsDictionary = value[Keys.settings] as? NSDictionary,
                  let isFaceIdAvailable = settingsDictionary[Keys.settingsInfo.faceId] as? Bool,
                  let isPushNotificationsAvailable = settingsDictionary[Keys.settingsInfo.pushNotifications] as? Bool,
                  let saveToFirebase = settingsDictionary[Keys.settingsInfo.firebase] as? Bool
            else { return }
            self.userData.settings = Settings(
                isFaceIdAvailable: isFaceIdAvailable,
                isPushNotificationsAvailable: isPushNotificationsAvailable,
                saveToFirebase: saveToFirebase
            )
            completion()
        })
    }
    
    func deleteUser() {
        guard let user = Auth.auth().currentUser else { return }
        user.delete { error in
            if let error = error {
                print(error)
            }
            print("deleted")
        }
    }
    
    func changePassword(to password: String, with completion: @escaping (String?) -> ()) {
        guard let user = Auth.auth().currentUser else { return }
        let hashPassword = HashMaker().MD5(string: password)
        let hashPasswordString = String(decoding: hashPassword, as: UTF8.self)
        user.updatePassword(to: hashPasswordString) { error in
            if let error = error {
                completion(error.localizedDescription)
            }
            completion(nil)
        }
    }
    
    func logOut() {
        try? Auth.auth().signOut()
    }
}
