import Reachability

protocol ConnectionDelegate: class {
    func connected(navigationTitle: String)
    func disconnected(navigationTitle: String)
}

enum ConnectionTitle {
    static let connected = "News"
    static let disconnected = "No Internet connection"
}

final class ReachabilityManager {
    
    private let reachability = try? Reachability()
    var delegate: ConnectionDelegate?
    
    init() {
        startNotifier()
        reachabilityChange()
    }

    deinit {
        stopNotifier()
    }
    
    private func startNotifier() {
        do {
            try reachability?.startNotifier()
        } catch {
            print("Unable to start notifier")
            return
        }
    }

    private func stopNotifier() {
        reachability?.stopNotifier()
    }

    private func reachabilityChange() {
        reachability?.whenReachable = { reachability in
            self.delegate?.connected(navigationTitle: ConnectionTitle.connected)
        }
        
        reachability?.whenUnreachable = { reachability in
            self.showConnectionView()
            self.delegate?.disconnected(navigationTitle: ConnectionTitle.disconnected)
        }
    }
    
    private func showConnectionView() {
        guard let connectionView = UIView.loadFromNibNamed(nibNamed: R.nib.internetConnection.name),
            let window = AppDelegate.shared.window
        else { return }
        
        connectionView.frame = CGRect(x: window.frame.width / 2 - 140,
                                      y: window.frame.height / 2 - 80,
                                      width: 280, height: 160)
        
        if UITraitCollection().userInterfaceStyle == .light {
            connectionView.backgroundColor = .gray
        }
        
        window.addSubview(connectionView)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            connectionView.fadeView {
                connectionView.removeFromSuperview()
            }
        }
    }
}
