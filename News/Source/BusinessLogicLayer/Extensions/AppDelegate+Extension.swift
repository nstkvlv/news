import UIKit
import CoreData

extension AppDelegate {
    
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as? AppDelegate ?? AppDelegate()
    }

    var managedContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    var rootViewController: UIViewController {
        if let window = window?.rootViewController {
            return window
        }
        return UIViewController()
    }
    
    func makeRootController(controller: UIViewController) {
        if let window = AppDelegate.shared.window {
            window.rootViewController = controller
        }
    }
}
