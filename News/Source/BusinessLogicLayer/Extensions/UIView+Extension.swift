import UIKit

extension UIView {
    func fadeView(toAlpha alpha: CGFloat = 0.0, withDuration duration: TimeInterval = 2.0, with completion: @escaping () -> () = { }) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = alpha
        }, completion: { (value: Bool) in
            completion()
        })
    }
    
    class func loadFromNibNamed(nibNamed: String, bundle: Bundle? = nil) -> UIView? {
      return UINib(nibName: nibNamed, bundle: bundle).instantiate(withOwner: nil, options: nil)[0] as? UIView
  }
}
