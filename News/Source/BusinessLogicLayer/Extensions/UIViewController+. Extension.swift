import UIKit

extension UIViewController {
    func createAlertController(title: String?, message: String?, style preferredStyle: UIAlertController.Style, actions: [UIAlertAction] = []) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        if actions.isEmpty {
            let okAction = UIAlertAction(title: "Ok", style: .default)
            alert.addAction(okAction)
        } else {
            actions.forEach { action in
                alert.addAction(action)
            }
        }
        alert.modalPresentationStyle = .overFullScreen
        return alert
    }
    
    func showAlert(title: String?, message: String?, style preferredStyle: UIAlertController.Style, actions: [UIAlertAction] = [], completion: (() -> Void)? = nil) {
        let alert = createAlertController(title: title, message: message, style: preferredStyle, actions: actions)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: completion)
        }
    }
}
