import UIKit

extension String {
    func withBoldText(text: String, font: UIFont = UIFont.systemFont(ofSize: 21, weight: .regular), boldTextFont: UIFont = UIFont.systemFont(ofSize: 21, weight: .regular)) -> NSAttributedString {
        let fullString = NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font: font])
        let boldFontAttribute: AttributesString = [NSAttributedString.Key.font: boldTextFont]
        let range = (self as NSString).range(of: text)
        fullString.addAttributes(boldFontAttribute, range: range)
        return fullString
    }
}


