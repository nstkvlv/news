import UIKit

extension UITextView {
    func attributedContent(for news: NewsStruct) {
        let newsContent = news.content
        let width = frame.width
        let content = "<style>* { max-width: \(width)px; height: auto; text-align: justify}</style>" + (newsContent)
        guard let data = content.data(using: .utf8) else { return }
        let htmlString: NSMutableAttributedString? = try? NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        if let attributedString = htmlString {
            let textRangeForFont: NSRange = NSMakeRange(0, attributedString.length)
            attributedString.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18)], range: textRangeForFont)
            attributedString.fixAttributes(in: textRangeForFont)
            attributedText = attributedString
            if #available(iOS 13.0, *) {
                textColor = .label
            } else {
                textColor = .black
            }
            isScrollEnabled = false
        }
    }
}
