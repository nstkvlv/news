import UIKit

extension UITextField {
    func setBottomBorder() {
        tintColor = R.color.appColors.mainColor()
        borderStyle = .none
        layer.backgroundColor = UIColor.white.cgColor

        layer.masksToBounds = false
        layer.shadowColor = R.color.appColors.mainColor()?.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 0.0
    }
    
//    func setColoredBottomBorder() {
//        layer.shadowColor = R.color.appColors.mainColor()?.cgColor.copy(alpha: 0.5)
//        layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
//    }
}
