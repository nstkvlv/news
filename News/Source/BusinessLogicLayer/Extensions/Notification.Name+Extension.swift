import Foundation

extension Notification.Name {
    static let allImagesDownloaded = Notification.Name("AllImagesDownloaded")
    static let clearArchivatedArray = Notification.Name("ClearArchivatedArray")
    static let hideLoadingView = Notification.Name("HideLoadingView")
}
