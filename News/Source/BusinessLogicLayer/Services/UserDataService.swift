import Foundation

private enum Keys {
    static let isLogged = "userIsLogged"
    static let isFaceIDAvailable = "isFaceIDAvailable"
    static let userID = "userID"
    static let userInfo = "userInfo"
    static let settings = "settings"
}

final class UserDataService {
    
    static let shared = UserDataService()
    private init() { }
    
    private var user = UserDefaults.standard
    
    var isLogged: Bool {
        get {
            user.bool(forKey: Keys.isLogged)
        }
        set {
            user.set(newValue, forKey: Keys.isLogged)
        }
    }
    
    var userInfo: UserInfo? {
        get {
            if let data = user.data(forKey: Keys.userInfo) {
                let userInfo = try? JSONDecoder().decode(UserInfo.self, from: data)
                return userInfo
            }
            return nil
        }
        set {
            let encoded = try? JSONEncoder().encode(newValue)
            user.setValue(encoded, forKey: Keys.userInfo)
        }
    }
    
    var settings: Settings? {
        get {
           if let data = user.data(forKey: Keys.settings) {
               let settings = try? JSONDecoder().decode(Settings.self, from: data)
               return settings
           }
           return nil
        }
        set {
           let encoded = try? JSONEncoder().encode(newValue)
           user.setValue(encoded, forKey: Keys.settings)
        }
    }
}
