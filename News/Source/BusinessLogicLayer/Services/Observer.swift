import UIKit

final class Observer {
    static let shared = Observer()
    private init() {}
    
    var achived = ["Create Account"]
    var achivmentsDictionary = ["save1"  : "Saved 1-st article",
                                "share1" : "Shared article with friend",
                                "read1"  : "Read 1-st article",
                                "use5"   : "Use app 5 days in a row",
                                "star10" : "Get 10 stars",
                                "read10" : "Read 10 articles",
                                "use10"  : "Use app 10 days in a row",
                                "share10": "Shared 10 articles with friends",
                                "star50" : "Get 50 stars"]
    
    var savedNewsAmount = 0 {
        didSet {
//            lookForAchivment(for: "save\(savedNewsAmount)", with: savedNewsAmount)
        }
    }
    
    var sharedNewsAmount = 0 {
        didSet {
            if sharedNewsAmount == 1 {
//                presentCongratulationView(with: "Shared first news".withBoldText(text: "first"))
            }
        }
    }
    
    var deletedNewsAmount = 0 {
        didSet {
            if deletedNewsAmount == 1 {
//                presentCongratulationView(with: "Deleted first news".withBoldText(text: "first"))
            }
        }
    }
    
    private enum Constants {
        static let smallDeviceWidthIndention: CGFloat = 30
        static let bigDeviceWidthIndention: CGFloat = 70
        static let smallDeviceHeightDivider: CGFloat = 4
        static let bigDeviceHeightDivider: CGFloat = 5
    }
    
    func lookForAchivment(for keyName: String, with BoldText: Int) {
        guard let achivmentName = achivmentsDictionary[keyName] else { return }
        let _ = achivmentsDictionary.map { key, value in
            if key == keyName {
                presentCongratulationView(with: (achivmentName + "!").withBoldText(text: "\(BoldText)"))
                achived.append(value)
                achivmentsDictionary.removeValue(forKey: key)
            }
        }
    }

    func presentCongratulationView(with text: NSAttributedString) {
        let congratulationsView = CongratulationView.loadFromNibNamed(nibNamed: R.nib.congratulationView.name)

        guard let congratulationView = congratulationsView as? CongratulationView,
              let window = AppDelegate.shared.window else { return }
        
        window.addSubview(congratulationView)
        
        congratulationView.achivmentLabel.attributedText = text
        
        if GlobalConstants.isSmallDevice {
            viewSetUp(on: window, customView: congratulationView, for: true,
                      widthIndention: Constants.smallDeviceWidthIndention,
                      heightDivider: Constants.smallDeviceHeightDivider)
        } else {
            viewSetUp(on: window, customView: congratulationView, for: false,
                      widthIndention: Constants.bigDeviceWidthIndention,
                      heightDivider: Constants.bigDeviceHeightDivider)
        }
        
        fireworks(on: window)
        
//        let confettiView = SAConfettiView(frame: window.frame)
//
//        window.addSubview(confettiView)
//        confettiView.intensity = 0.7
//        confettiView.type = .Star
//        confettiView.startConfetti()

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
//            confettiView.stopConfetti {
//                confettiView.fadeView()
            congratulationView.fadeView() {
                congratulationView.removeFromSuperview()
//                    confettiView.removeFromSuperview()
            }
        }
//      }
    }

    func viewSetUp(on window: UIWindow, customView: CongratulationView, for smallDevice: Bool, widthIndention: CGFloat, heightDivider: CGFloat) {
        if smallDevice {
            customView.congratulationLabel.font = .boldSystemFont(ofSize: 22)
        }

        customView.layer.masksToBounds = true
        customView.layer.borderColor = UIColor.white.cgColor
        customView.layer.borderWidth = 0.3
        customView.layer.cornerRadius = 20
        
        customView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            customView.widthAnchor.constraint(equalToConstant: window.frame.width - widthIndention),
            customView.heightAnchor.constraint(equalToConstant: window.frame.height/heightDivider),
            customView.centerXAnchor.constraint(equalTo: window.centerXAnchor),
            customView.centerYAnchor.constraint(equalTo: window.centerYAnchor)
        ])
    }
    
    func fireworks(on window: UIWindow) {
        let size = CGSize(width: window.frame.width, height: window.frame.height)

        let particlesLayer = CAEmitterLayer()
        particlesLayer.frame = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)

        window.layer.addSublayer(particlesLayer)
        window.layer.masksToBounds = true

        particlesLayer.backgroundColor = UIColor.clear.cgColor
        particlesLayer.emitterShape = .point
        particlesLayer.emitterPosition = CGPoint(x: window.frame.width / 2,
                                                 y: window.frame.height - 50)
        particlesLayer.emitterSize = CGSize(width: 0.0, height: 0.0)
        particlesLayer.emitterMode = .outline
        particlesLayer.renderMode = .additive


        let cell1 = CAEmitterCell()

        cell1.name = "Parent"
        cell1.birthRate = 5.0
        cell1.lifetime = 2.5
        cell1.velocity = 300.0
        cell1.velocityRange = 100.0
        cell1.yAcceleration = -100.0
        cell1.emissionLongitude = -90.0 * (.pi / 180.0)
        cell1.emissionRange = 45.0 * (.pi / 180.0)
        cell1.scale = 0.0
        cell1.color = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        cell1.redRange = 0.9
        cell1.greenRange = 0.9
        cell1.blueRange = 0.9



        let image1_1 = UIImage(named: "spark")?.cgImage

        let subcell1_1 = CAEmitterCell()
        subcell1_1.contents = image1_1
        subcell1_1.name = "Trail"
        subcell1_1.birthRate = 45.0
        subcell1_1.lifetime = 0.5
        subcell1_1.beginTime = 0.01
        subcell1_1.duration = 1.7
        subcell1_1.velocity = 80.0
        subcell1_1.velocityRange = 100.0
        subcell1_1.xAcceleration = 100.0
        subcell1_1.yAcceleration = 350.0
        subcell1_1.emissionLongitude = -360.0 * (.pi / 180.0)
        subcell1_1.emissionRange = 22.5 * (.pi / 180.0)
        subcell1_1.scale = 0.5
        subcell1_1.scaleSpeed = 0.13
        subcell1_1.alphaSpeed = -0.7
        subcell1_1.color = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor



        let image1_2 = UIImage(named: "spark")?.cgImage

        let subcell1_2 = CAEmitterCell()
        subcell1_2.contents = image1_2
        subcell1_2.name = "Firework"
        subcell1_2.birthRate = 20000.0
        subcell1_2.lifetime = 15.0
        subcell1_2.beginTime = 1.6
        subcell1_2.duration = 0.1
        subcell1_2.velocity = 190.0
        subcell1_2.yAcceleration = 80.0
        subcell1_2.emissionRange = 360.0 * (.pi / 180.0)
        subcell1_2.spin = 114.6 * (.pi / 180.0)
        subcell1_2.scale = 0.1
        subcell1_2.scaleSpeed = 0.09
        subcell1_2.alphaSpeed = -0.7
        subcell1_2.color = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor

        cell1.emitterCells = [subcell1_1, subcell1_2]

        particlesLayer.emitterCells = [cell1]
    }
}



