import UIKit

final class HomeModel {
    
    // MARK: - Properties
    
    let session = AppDelegate.shared.session
    let user = UserDataService.shared
    let notification = NotificationCenter.default
    var selectedCellsIndexPath = [IndexPath]()
    var currentArr = [NewsStruct]()
    var isDownloaded = false
    
    // MARK: - Methods
    
    func moveItem(on sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = currentArr.remove(at: sourceIndexPath.item)
        currentArr.insert(item, at: destinationIndexPath.item)
        session.savedNews = currentArr.reversed()
    }
    
    func setNotificationAllImagesDownloaded(with completion: @escaping() -> ()) {
        notification.addObserver(forName: .allImagesDownloaded, object: nil, queue: nil) { [weak self] _ in
            guard let self = self else { return }
            self.isDownloaded = true
            DispatchQueue.main.async {
                completion()
            }
        }
    }
    
    func itemsToDelete() -> [IndexPath] {
        let indexPathItems = selectedCellsIndexPath.map { $0.item }
        currentArr = currentArr
            .enumerated()
            .filter({
                !indexPathItems.contains($0.offset)
            })
            .map { $0.element }
        session.savedNews = currentArr.reversed()
        return selectedCellsIndexPath
    }
}
