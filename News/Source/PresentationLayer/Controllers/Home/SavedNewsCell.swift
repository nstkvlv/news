import UIKit

final class SavedNewsCell: UICollectionViewCell {

    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var loadView: UIView!
    @IBOutlet weak var checkMarkImage: UIImageView!
}
