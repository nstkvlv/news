import UIKit

final class AchivmentsViewController: UIViewController {
    
    // MARK: - Outlets

    @IBOutlet private weak var tableView: UITableView!
    
    // MARK: - Properties
    
    private let observer = Observer.shared
    private let sections = ["Achived", "Goals"]
        
    // MARK: - Methods
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
            tableView.reloadData()
        }
    }
}

extension AchivmentsViewController: UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView,
              let headerLabel = header.textLabel else {return }
        headerLabel.font = .systemFont(ofSize: 22.0)
        headerLabel.textColor = .darkGray
        if #available(iOS 13.0, *) {
            headerLabel.textColor = .label
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

extension AchivmentsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return observer.achived.count
        case 1:
            return observer.achivmentsDictionary.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.achivmentsCell, for: indexPath)
            else { return UITableViewCell() }
        cell.starView.borderColor = .lightGray
        if #available(iOS 13.0, *), traitCollection.userInterfaceStyle == .dark {
            cell.starView.fillColor = .systemGray6
        } else {
            cell.starView.fillColor = .white
        }
        
        switch indexPath.section {
        case 0:
            guard let color = R.color.appColors.starAchivmentColor() else { return cell }
            cell.starView.fillColor = color
            cell.starView.borderColor = .clear
            cell.achivmentLabel.text = observer.achived[indexPath.row]
        case 1:
            cell.achivmentLabel.text = Array(observer.achivmentsDictionary.values.sorted())[indexPath.row]
        default:
            break
        }
        return cell
    }
}
