import UIKit

class StarView: UIView {
    private var cornerRadius: CGFloat = 1.8 {
        didSet {
            setNeedsDisplay()
        }
    }
    private var rotation: CGFloat = 54 {
        didSet {
            setNeedsDisplay()
        }
    }
    var borderColor: UIColor = .lightGray {
        didSet {
            setNeedsDisplay()
        }
    }
    var fillColor: UIColor = R.color.appColors.starAchivmentColor() ?? .white {
        didSet {
            setNeedsDisplay()
        }
    }

    override func draw(_ rect: CGRect) {
    
        let path = UIBezierPath()
        let center = CGPoint(x: rect.width / 2, y: rect.height / 2)
        let R = rect.width / 2
        let cornerR = cornerRadius
        let roundingR = R * 0.95 - cornerR
        var cangle = rotation
        
        for i in 1 ... 5 {
            let cc = CGPoint(x: center.x + roundingR * cos(cangle * .pi / 180), y: center.y + roundingR * sin(cangle * .pi / 180))
            let p = CGPoint(x: cc.x + cornerR * cos((cangle - 72) * .pi / 180), y: cc.y + cornerR * sin((cangle - 72) * .pi / 180))
            
            if i == 1 {
                path.move(to: p)
            } else {
                path.addLine(to: p)
            }

            path.addArc(withCenter: cc, radius: cornerR, startAngle: (cangle - 72) * .pi / 180, endAngle: (cangle + 72) * .pi / 180, clockwise: true)

            cangle += 144
        }
        path.close()
        
        borderColor.setStroke()
        path.stroke()

        fillColor.setFill()
        path.fill()
    }
    
}
