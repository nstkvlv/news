import UIKit

final class HomeView: UIView {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var collectionViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet private weak var collectionViewUpConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var achivmentsAmount: UILabel!
    @IBOutlet weak var newsAmountLabel: UILabel!
    @IBOutlet weak var achivmentsLabel: UILabel!
    @IBOutlet weak var lastChangeLabel: UILabel!
    @IBOutlet weak var newsAmount: UILabel!
    @IBOutlet weak var lastChange: UILabel!
    
    // MARK: - Properties
    
    let deleteButton = UIButton(type: .custom)
    let blurView = CommonModel.addBlurView()

    var lastContentOffset: CGFloat = 0
    var isShownCheckMark = false
    
    private enum Constants {
        static let deleteButtonTrailingBottomConstraint: CGFloat = 30
        static let deleteButtonHeigtWidth: CGFloat = 65
        static let searchBarHeight: CGFloat = 50
        static let collectionViewVerticalSapcingToInfo: CGFloat = 2
        static let animationTime = 0.25
    }
    
    // MARK: - UIView lifecycle methods

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.allowsMultipleSelection = true
    }
    
    // MARK: - Methods
    
    func moveCellStates(longPress: UILongPressGestureRecognizer) {
        switch longPress.state {
           case .began:
               guard let selectedIndexPath = collectionView.indexPathForItem(at: longPress.location(in: collectionView)) else { break }
               collectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
           case .changed:
               collectionView.updateInteractiveMovementTargetPosition(longPress.location(in: longPress.view))
           case .ended:
               collectionView.endInteractiveMovement()
           default:
               collectionView.cancelInteractiveMovement()
       }
    }
    
    func hideSearchBar(contentOffsetY: CGFloat) {
        lastContentOffset = contentOffsetY
        collectionViewUpConstraint.constant = Constants.collectionViewVerticalSapcingToInfo
        UIView.animate(
            withDuration: Constants.animationTime,
            delay: 0,
            options: .curveEaseOut,
            animations: {
                self.layoutIfNeeded()
            },
            completion: { [weak self] _ in
                self?.searchBar.isHidden = true
        })
    }
    
    func showSearchBar() {
        lastContentOffset = 0
        collectionViewUpConstraint.constant = Constants.searchBarHeight
        searchBar.isHidden = false
        UIView.animate(
           withDuration: Constants.animationTime,
           delay: 0,
           options: .curveEaseInOut,
           animations: { [weak self] in
            self?.layoutIfNeeded()
           })
    }
    
    func setUpViewBeforeDeleteAction() {
        isShownCheckMark = false
        collectionViewBottomConstraint.constant = 0
        deleteButton.removeFromSuperview()
        collectionView.reloadData()
    }
    
    func setUpEditAppearance(tabBarHeight: CGFloat, with completion: (UIButton) -> ()) {
        isShownCheckMark = true
        collectionView.reloadData()
        addDeleteButton { button in
            completion(button)
        }
    }
    
    func colorPressedLabel(titleLabel: UILabel, amountLabel: UILabel, to color: UIColor) {
        var labelTextColor = color
        if #available(iOS 13.0, *), labelTextColor == .black {
            labelTextColor = .label
        }
        if traitCollection.userInterfaceStyle == .light, labelTextColor == .lightText {
            labelTextColor = .darkGray
       }
        titleLabel.textColor = labelTextColor
        amountLabel.textColor = labelTextColor
    }
    
    func addDeleteButton(with completion: (_ button: UIButton) -> ()) {
        deleteButton.backgroundColor = .clear
        deleteButton.setImage(R.image.appImages.deleteButton(), for: .normal)
        deleteButton.contentHorizontalAlignment = .fill
        deleteButton.contentVerticalAlignment = .fill
    
        self.addSubview(deleteButton)
        addConstraints(for: deleteButton)
        completion(deleteButton)
    }
    
    func addConstraints(for deleteButton: UIButton) {
        deleteButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
           trailingAnchor.constraint(equalTo: deleteButton.trailingAnchor,
                                          constant: Constants.deleteButtonTrailingBottomConstraint),
           bottomAnchor.constraint(equalTo: deleteButton.bottomAnchor,
                                        constant: Constants.deleteButtonTrailingBottomConstraint),
           deleteButton.widthAnchor.constraint(equalToConstant: Constants.deleteButtonHeigtWidth),
           deleteButton.heightAnchor.constraint(equalToConstant: Constants.deleteButtonHeigtWidth)
        ])
    }
    
    func showAlertToLogIn(swithTabBar completion: @escaping() -> ()) -> UIAlertController {
        let logInAction = UIAlertAction(title: "Log In", style: .default, handler: { _ in
            guard let VC = R.storyboard.auth.logIn() else { return }
            AppDelegate.shared.makeRootController(controller: VC)
        })
        let cancelAction = (UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            completion()
        }))
        
        logInAction.setValue(R.color.appColors.mainColor(), forKey: "titleTextColor")
        cancelAction.setValue(R.color.appColors.mainColor(), forKey: "titleTextColor")
        
        let alert = UIViewController().createAlertController(title: nil, message: nil, style: .actionSheet, actions: [logInAction, cancelAction])
        let titleString = "To use all the capabilities of the app you should registrate"
        alert.setValue(titleString.withBoldText(text: titleString), forKey: "attributedTitle")
        
        return alert
    }
}
