import UIKit

private enum viewAppearance {
    case normal
    case delete
}

final class HomeViewController: UIViewController {
    
    // MARK: - Properties

    private let longPress = UILongPressGestureRecognizer()
    private let model = HomeModel()
    
    // MARK: - Constants

    private enum Constants {
        static let pressDuration = 0.5
        static let cellWidthDifference: CGFloat = 5
        static let cellHeightDifference: CGFloat = 10
        static let scrollViewTopContentOffsetY: CGFloat = -20
        static let cornerRadius: CGFloat = 8
    }
        
    // MARK: - View controller lifecycle methods
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        model.setNotificationAllImagesDownloaded {
            if let collection = self.view().collectionView {
                self.model.currentArr = self.model.session.savedNews.reversed()
                collection.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarItems(for: .normal)
        addLongPress()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        model.currentArr = model.session.savedNews.reversed()

        if !model.user.isLogged {
            title = nil
            let alert = view().showAlertToLogIn( swithTabBar: {
                self.tabBarController?.selectedIndex = 0
            })
            present(alert, animated: true)
        } else {
            title = model.user.userInfo?.name
            view().lastChange.text  = model.user.userInfo?.lastChangesDate
            view().newsAmount.text = String(model.session.savedNews.count)
        }
        view().collectionView.reloadData()
    }
    
    deinit {
        model.notification.removeObserver(self)
    }
    
    // MARK: - Methods
    
    func view() -> HomeView {
        let homeView = view as? HomeView
        return homeView ?? HomeView()
    }

    private func addLongPress() {
        longPress.addTarget(self, action: #selector(moveCell))
        longPress.minimumPressDuration = Constants.pressDuration
        view().collectionView.addGestureRecognizer(longPress)
    }
    
    private func setNavigationBarItems(for condition: viewAppearance) {
        switch condition {
            case .delete:
                navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelPush))
                navigationItem.leftBarButtonItem?.isEnabled = false
                navigationItem.leftBarButtonItem?.tintColor = .clear
            case .normal:
                navigationItem.rightBarButtonItem = UIBarButtonItem(image: R.image.systemIcons.settingsGear(), style: .plain, target: self, action: #selector(settingsPush))
                navigationItem.rightBarButtonItem?.tintColor = R.color.appColors.buttonColor()
                navigationItem.leftBarButtonItem?.isEnabled = true
                navigationItem.leftBarButtonItem?.tintColor = R.color.appColors.buttonColor()
        }
    }
    
    private func returnViewBeforeDeleteAction() {
        model.selectedCellsIndexPath.removeAll()
        longPress.isEnabled = true
        tabBarController?.tabBar.isHidden = false
        view().setUpViewBeforeDeleteAction()
    }
    
    // MARK: - Actions
    
    @IBAction func settingsPush() {
        if let VC = R.storyboard.settings.settingsViewController() {
            navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    @IBAction func deleteSeveral() {
        view().collectionView.performBatchUpdates({
            view().collectionView.deleteItems(at: model.itemsToDelete())
        }) { [weak self] _ in
            guard let self = self else { return }
            self.model.user.userInfo?.lastChangesDate = CommonModel.getCurrentDate()
            self.view().newsAmount.text = String(self.model.session.savedNews.count)
            self.returnViewBeforeDeleteAction()
            self.setNavigationBarItems(for: .normal)
        }
    }
    
    @IBAction func cancelPush() {
        setNavigationBarItems(for: .normal)
        returnViewBeforeDeleteAction()
    }
    
    @IBAction func moveCell(longPress: UILongPressGestureRecognizer) {
        view().moveCellStates(longPress: longPress)
    }
    
    @IBAction func editPush(_ sender: UIBarButtonItem) {
        guard let tabBar = tabBarController?.tabBar else { return }
        tabBar.isHidden = true
        longPress.isEnabled = false
        setNavigationBarItems(for: .delete)
        view().setUpEditAppearance(tabBarHeight: tabBar.frame.height) { button in
            button.addTarget(self, action: #selector(deleteSeveral), for: .touchUpInside)
        }
        view().layoutIfNeeded()
    }
    
    @IBAction func achivmentsButtonTouchDown(_ sender: UIButton) {
        view().colorPressedLabel(titleLabel: view().achivmentsLabel, amountLabel: view().achivmentsAmount, to: .lightText)
    }

    @IBAction func newsAmountButtonTouchDown(_ sender: UIButton) {
        view().colorPressedLabel(titleLabel: view().newsAmountLabel, amountLabel: view().newsAmount, to: .lightText)
    }
    
    @IBAction func lastChangeButtonTouchDown(_ sender: UIButton) {
        view().colorPressedLabel(titleLabel: view().lastChangeLabel, amountLabel: view().lastChange, to: .lightText)
    }
    
    @IBAction func achivmentsButtonPush(_ sender: UIButton) {
        view().colorPressedLabel(titleLabel: view().achivmentsLabel, amountLabel: view().achivmentsAmount, to: .black)
        if let VC = R.storyboard.home.achivmentsViewController() {
            navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    @IBAction func newsAmountButtonPush(_ sender: UIButton) {
        view().colorPressedLabel(titleLabel: view().newsAmountLabel, amountLabel: view().newsAmount, to: .black)
    }
    
    @IBAction func lastChangeButtonPush(_ sender: UIButton) {
        view().colorPressedLabel(titleLabel: view().lastChangeLabel, amountLabel: view().lastChange, to: .black)
    }
}

extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if view().isShownCheckMark {
            guard let cell = collectionView.cellForItem(at: indexPath) as? SavedNewsCell else { return }
            cell.checkMarkImage.image = R.image.appImages.checkMark()
            model.selectedCellsIndexPath.append(indexPath)
            collectionView.performBatchUpdates(nil)
            return
        }
        
        if let VC = R.storyboard.newsInfo.presentNewsViewController() {
            let navigationController = UINavigationController(rootViewController: VC)
            navigationController.navigationBar.isHidden = true
            VC.news = model.currentArr[indexPath.item]
            VC.modalPresentationStyle = .popover
            view().blurView.isHidden = false
            present(navigationController, animated: true, completion: {
                collectionView.deselectItem(at: indexPath, animated: false)
                self.view().blurView.isHidden = true
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if view().isShownCheckMark {
            guard let cell = collectionView.cellForItem(at: indexPath) as? SavedNewsCell else { return }
            cell.checkMarkImage.image = R.image.systemIcons.circle()
            model.selectedCellsIndexPath = model.selectedCellsIndexPath.filter({
                $0 != indexPath
            })
            collectionView.performBatchUpdates(nil)
        }
    }
}

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.currentArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.savedNewsCell, for: indexPath)
            else { return UICollectionViewCell() }
        cell.title.text = model.currentArr[indexPath.item].title
        cell.newsImage.roundImage(cornerRadius: Constants.cornerRadius)
        cell.checkMarkImage.isHidden = true
        if model.isDownloaded {
            cell.isUserInteractionEnabled = true
            cell.loadView.isHidden = true
            cell.newsImage.image = model.currentArr[indexPath.item].image
        }
        if view().isShownCheckMark {
            cell.checkMarkImage.isHidden = false
            let isContainsIndex = model.selectedCellsIndexPath
                .map { $0.item }
                .contains(indexPath.item)
            cell.checkMarkImage.image = isContainsIndex ? R.image.appImages.checkMark() : R.image.systemIcons.circle()
        }
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let hasText = view().searchBar.text?.isEmpty, hasText else { return }
        if scrollView.contentOffset.y < Constants.scrollViewTopContentOffsetY || model.currentArr.count < 5 {
            view().showSearchBar()
            return
        }
        if view().lastContentOffset < scrollView.contentOffset.y {
            view().hideSearchBar(contentOffsetY: scrollView.contentOffset.y)
        }
    }
    
     func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        model.moveItem(on: sourceIndexPath, to: destinationIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if model.selectedCellsIndexPath.contains(indexPath) {
            return CGSize(width: view.frame.width / 2 - Constants.cellWidthDifference - 10, height: view.frame.height / 4 - Constants.cellHeightDifference - 10)
        }
        return CGSize(width: view.frame.width / 2 - Constants.cellWidthDifference, height: view.frame.height / 4 - Constants.cellHeightDifference)
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        model.currentArr = searchText.isEmpty ? model.session.savedNews : model.session.savedNews.filter({ $0.title.lowercased().contains(searchText.lowercased()) })
        DispatchQueue.main.async {
            self.view().collectionView.reloadData()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
        
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        model.currentArr = model.session.savedNews
        DispatchQueue.main.async {
            self.view().collectionView.reloadData()
        }
        searchBar.resignFirstResponder()
    }
}

extension HomeViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view().searchBar.resignFirstResponder()
    }
}
