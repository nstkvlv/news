import UIKit
import MessageUI

final class SettingsViewController: UIViewController {
    
    // MARK: - Outlets

    @IBOutlet private weak var pushNotificationswitch: UISwitch!
    @IBOutlet private weak var firebaseSwitch: UISwitch!
    @IBOutlet private weak var faceIDSwitch: UISwitch!
    @IBOutlet private weak var enableFaceIDButton: UIButton!
    @IBOutlet private weak var nameButton: UIButton!
    @IBOutlet private weak var passwordArrow: UIImageView!
    @IBOutlet private weak var emailLabel: UILabel!
    
    // MARK: - Properties
    
    private let biometricAuthentication = BiometricAuthentication()
    private let model = SettingsModel()

    // MARK: - View controller lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViewItems()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        model.user.settings = Settings(isFaceIdAvailable: faceIDSwitch.isOn,
                                       isPushNotificationsAvailable: pushNotificationswitch.isOn,
                                       saveToFirebase: firebaseSwitch.isOn)
    }
    
    // MARK: - Methods
    
    private func setUpViewItems() {
        emailLabel.text = model.user.userInfo?.email
        nameButton.setTitle(model.user.userInfo?.name, for: .normal)
        faceIDSwitch.isOn = biometricAuthentication.isFaceIDAvailableInApp()
        
        if let isNotificationsAvailable = model.user.settings?.isPushNotificationsAvailable,
           let saveToFirebase = model.user.settings?.saveToFirebase {
            pushNotificationswitch.isOn = isNotificationsAvailable
            firebaseSwitch.isOn = saveToFirebase
        }
        
        if !biometricAuthentication.isFaceIDAvailiable {
           enableFaceIDButton.isEnabled = true
        }
    }
    
    private func showNewsViewController() {
        tabBarController?.selectedIndex = 0
        navigationController?.popViewController(animated: false)
        model.user.settings = nil
    }
    
    // MARK: - Actions
    
    @IBAction func editNamePush(_ sender: UIButton) {
        model.createAlertWithTextField(for: sender)
    }
    
    @IBAction func changePasswordPush() {
        model.createalertToChangePassword()
    }
    
    @IBAction func enableFaceID() {
        model.openSettings()
    }
    
    @IBAction func saveAccountSwitched(_ sender: UISwitch) {
        guard sender.isOn,
              let userInfo = model.user.userInfo,
              let userId = userInfo.userID,
              let passwotd = CoreDataHelper().getUserPasword(for: userId)
        else { return }
        model.user.settings = Settings(isFaceIdAvailable: faceIDSwitch.isOn,
                                       isPushNotificationsAvailable: pushNotificationswitch.isOn,
                                       saveToFirebase: firebaseSwitch.isOn)
        FirebaseHelper().registerToFirebase(email: userInfo.email, password: passwotd, with: { error in
            if let error = error {
                sender.isOn = false
                print(error)
                return
            }
            FirebaseHelper().addSavedNews(saveArray: self.model.saveArray)
        })
    }
    
    @IBAction func supportButtonPush(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([Util.mail])
            present(mail, animated: true)
        } else {
            showAlert(title: "Error", message: "Check your Email account", style: .alert)
        }
    }
    
    @IBAction func logOutPush() {
        model.user.settings = Settings(isFaceIdAvailable: faceIDSwitch.isOn,
                                       isPushNotificationsAvailable: pushNotificationswitch.isOn,
                                       saveToFirebase: firebaseSwitch.isOn)
        CoreDataHelper().addSettings()
        model.saveNewsToCoreData()
        
        if firebaseSwitch.isOn {
            FirebaseHelper().addInfoToUser()
            FirebaseHelper().addSavedNews(saveArray: model.saveArray)
        }
        
        model.logOutAction()
        showNewsViewController()
    }
    
    @IBAction func deletePush() {
        CoreDataHelper().deleteCurrentUser(with: { [weak self] in
            guard let self = self else { return }
            FirebaseHelper().deleteUser()
            self.model.logOutAction()
            self.showNewsViewController()
        })
    }
}

extension SettingsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
