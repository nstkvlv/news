import UIKit

final class SettingsModel {
    
    // MARK: - Properties
    
    private let session = AppDelegate.shared.session
    let user = UserDataService.shared
    var saveArray: [SavedNewsStruct] {
        var array = [SavedNewsStruct]()
        session.savedNews.forEach { news in
            array.append(SavedNewsStruct(title: news.title,
                                         author: news.author,
                                         content: news.content,
                                         imageUrl: news.imageUrl,
                                         url: news.url))
        }
        return array
    }
    
    // MARK: - Methods

    func saveNewsToCoreData() {
        if user.isLogged {
            CoreDataHelper().saveNews(saveArray: saveArray)
            guard let date = user.userInfo?.lastChangesDate else { return }
            CoreDataHelper().changeInfo(to: date, type: .lastChangesDate)
        }
    }
        
    func createAlertWithTextField(for button: UIButton) {
        let title = "Edit"
        let message = "Change name"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addTextField { textField in
            textField.placeholder = "Enter new name"
            textField.font = .systemFont(ofSize: 17)
        }
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { [weak self] _ in
            guard let self = self,
                  let newData = alert.textFields?.first?.text,
                  !newData.isEmpty else { return }
            CoreDataHelper().changeInfo(to: newData, type: .name)
            self.user.userInfo?.name = newData
            button.setTitle(newData, for: .normal)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        guard let window = AppDelegate.shared.window else { return }
        window.rootViewController?.present(alert, animated: true)
    }
    
    func createalertToChangePassword() {
        let title = "Change Password"
        let message = "Password must be more than 6 charachters."
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addTextField { textField in
           textField.placeholder = "password"
           textField.font = .systemFont(ofSize: 17)
           textField.isSecureTextEntry = true
        }

        alert.addTextField { textField in
           textField.placeholder = "repeat password"
           textField.font = .systemFont(ofSize: 17)
           textField.isSecureTextEntry = true
        }
               
        guard let window = AppDelegate.shared.window else { return }
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
           guard let password = alert.textFields?.first?.text,
                 let repeatPassword = alert.textFields?.last?.text else { return }
            
            if !password.isEmpty, password == repeatPassword {
                CoreDataHelper().changeInfo(to: password, type: .password)
                FirebaseHelper().changePassword(to: password) { error in
                    if let error = error {
                        alert.message = error
                        alert.textFields?.forEach({ textField in
                            textField.text = nil
                        })
                        window.rootViewController?.present(alert, animated: true)
                    }
                }
            } else {
               alert.message = "Password mismatch"
               alert.textFields?.forEach({ textField in
                   textField.text = nil
               })
            window.rootViewController?.present(alert, animated: true)
           }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)

        alert.addAction(cancelAction)
        alert.addAction(okAction)
        window.rootViewController?.present(alert, animated: true)
    }
    
    func openSettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString),
              UIApplication.shared.canOpenURL(url) else { return }
        UIApplication.shared.open(url)
    }
    
    func logOutAction() {
        FirebaseHelper().logOut()
        user.isLogged = false
        user.userInfo = nil
        session.savedNews.removeAll()
        NotificationCenter.default.post(name: .clearArchivatedArray, object: self)
    }
}
