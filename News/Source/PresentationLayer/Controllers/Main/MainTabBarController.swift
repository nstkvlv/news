import UIKit

final class MainTabBarController: UITabBarController {

    // MARK: - Properties

    private var previousController: UIViewController?

    // MARK: - View controller lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
}

extension MainTabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if previousController == viewController {
            switch tabBarController.selectedIndex {
                case 0:
                    guard let navigationController = viewController as? UINavigationController,
                          let newsVC = navigationController.viewControllers[0] as? NewsViewController else { return }
                    newsVC.view().tableView.setContentOffset(CGPoint.zero, animated: true)
                case 1:
                    guard let navigationController = viewController as? UINavigationController,
                          let homeVC = navigationController.viewControllers[0] as? HomeViewController else { return }
                    homeVC.view().collectionView.setContentOffset(CGPoint.zero, animated: true)
                default:
                    return
            }
        }
        previousController = viewController
    }
}
