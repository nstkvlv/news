import UIKit

final class StartScreenViewController: UIViewController {
    
    // MARK: - Properties

    private let biometricAuthentication = BiometricAuthentication()
    private var isFirstRun = true
    
    // MARK: - View controller lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        biometricAuthentication.authenticate(
           onSuccess: {
               guard let VC = R.storyboard.main.mainTabBarController() else { return }
               AppDelegate.shared.makeRootController(controller: VC)
        }, onCancel: {
               self.biometricAuthentication.showBlockScreen()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isFirstRun || !biometricAuthentication.isFaceIDAvailableInApp() {
            guard let VC = R.storyboard.main.mainTabBarController() else { return }
            AppDelegate.shared.makeRootController(controller: VC)
        }
        isFirstRun = false
    }
}

