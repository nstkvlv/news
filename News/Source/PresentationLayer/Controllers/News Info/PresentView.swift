import UIKit

final class PresentView: UIView {

    // MARK: - Outlets
    
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var urlButton: UIButton!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    // MARK: - Properties
       
   let floatingButton = UIButton(type: .custom)
   let saveString = "Saved"

   // MARK: - Constants
   
    private enum Constants {
       static let floatingButtonTrailingConstraint: CGFloat = 25.0
       static let floatingButtonBottomConstraint: CGFloat = 30.0
       static let floatingButtonWidthHeight: CGFloat = 45.0
       static let floatingButtonBorderWidth: CGFloat = 0.5
       static let blurViewAlpha: CGFloat = 0.9
       static let saveViewCornerRadius: CGFloat = 25
       static let saveViewWidth: CGFloat = 180
       static let saveViewHeight: CGFloat = 55
       static let saveViewAlpha: CGFloat = 0.9
       static let saveViewYUp: CGFloat = 65
       static let fontSize: CGFloat = 27
   }

       
   // MARK: - Methods
   
    func fillInInformation(news: NewsStruct) {
        authorLabel.text = CommonModel.getAuthor(for: news)
        titleLabel.text = news.title
        newsImage.image = news.image
        urlButton.setTitle(news.url, for: .normal)
        contentTextView.attributedContent(for: news)
    }

   func setAppearance(to navigationBar: UINavigationBar) {
       navigationBar.prefersLargeTitles = false
       navigationBar.isHidden = false
   }
   
   func createFloatingButton(with completion: (_  button: UIButton) -> ()) {
       floatingButton.layer.masksToBounds = true
       floatingButton.layer.cornerRadius = Constants.floatingButtonWidthHeight / 2
       floatingButton.layer.borderWidth = Constants.floatingButtonBorderWidth
       floatingButton.layer.borderColor = UIColor.systemGray.cgColor
       floatingButton.backgroundColor = .clear

       let image = R.image.systemIcons.arrow()?.withRenderingMode(.alwaysTemplate)
       floatingButton.setImage(image, for: .normal)
       floatingButton.tintColor = R.color.appColors.buttonColor()

       createBlurBackground(for: floatingButton)
       completion(floatingButton)

       self.addSubview(floatingButton)
       floatingButtonConstraints(for: floatingButton)

       floatingButton.isHidden = true
    }
   
   func createBlurBackground(for button: UIButton) {
       let blur = UIVisualEffectView(effect: UIBlurEffect(style: .prominent))
       blur.frame = button.bounds
       blur.alpha = Constants.blurViewAlpha
       blur.isUserInteractionEnabled = false
       blur.autoresizingMask = [.flexibleWidth, .flexibleHeight]
       button.insertSubview(blur, at: 0)
       if let imageView = button.imageView {
           button.bringSubviewToFront(imageView)
       }
   }
   
   func floatingButtonConstraints(for button: UIButton) {
         button.translatesAutoresizingMaskIntoConstraints = false
         NSLayoutConstraint.activate([
             trailingAnchor.constraint(equalTo: button.trailingAnchor, constant: Constants.floatingButtonTrailingConstraint),
             bottomAnchor.constraint(equalTo: button.bottomAnchor, constant: Constants.floatingButtonBottomConstraint),
             button.widthAnchor.constraint(equalToConstant: Constants.floatingButtonWidthHeight),
             button.heightAnchor.constraint(equalToConstant: Constants.floatingButtonWidthHeight)
         ])
   }
   
   func createSaveView() -> UIView {
        let saveView = UIView()
        saveView.backgroundColor = .white
        saveView.layer.cornerRadius = Constants.saveViewCornerRadius
        saveView.alpha = Constants.saveViewAlpha
        self.addSubview(saveView)

        saveView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.centerYAnchor.constraint(equalTo: saveView.centerYAnchor, constant: self.frame.height / 4 + Constants.saveViewYUp),
            self.centerXAnchor.constraint(equalTo: saveView.centerXAnchor),
            saveView.heightAnchor.constraint(equalToConstant: Constants.saveViewHeight),
            saveView.widthAnchor.constraint(equalToConstant: Constants.saveViewWidth)
        ])

        let saveLabel = UILabel()
        saveLabel.frame = saveView.frame
        saveLabel.text = saveString
        saveLabel.textColor = .black
        saveLabel.textAlignment = .center
        saveLabel.font = .boldSystemFont(ofSize: Constants.fontSize)
        saveLabel.translatesAutoresizingMaskIntoConstraints = false
        saveView.addSubview(saveLabel)
        saveLabel.centerXAnchor.constraint(equalTo: saveView.centerXAnchor).isActive = true
        saveLabel.centerYAnchor.constraint(equalTo: saveView.centerYAnchor).isActive = true
        
        return saveView
    }
}
