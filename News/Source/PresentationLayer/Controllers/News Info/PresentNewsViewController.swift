import UIKit

final class PresentNewsViewController: UIViewController {
    
    // MARK: - Properties
    
    private let session = AppDelegate.shared.session
    private let observer = Observer.shared
    private let user = UserDataService.shared
    private var lastContentOffset: CGFloat = 0
    private var isModalPresentationPopOver: Bool {
        return self.modalPresentationStyle == .popover
    }
    private var isNewsAlreadySaved: Bool {
        return session.savedNews.contains(where: { news -> Bool in
            self.news?.title == news.title
        })
    }
    
    var news: NewsStruct?

    // MARK: - Constants
    
    private enum Constants {
        static let showFloatingButtonY: CGFloat = 200
        static let contentOffsetY: CGFloat = 100
    }

    // MARK: - View controller lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view().createFloatingButton { button in
            button.addTarget(self, action: #selector(floatingButtonPush), for: .touchUpInside)
        }
        guard let news = news else { return }
        view().fillInInformation(news: news)

        if !user.isLogged {
            view().addButton.tintColor = .gray
        }

        if isModalPresentationPopOver {
            createBarButtons()
        }
        
        if isNewsAlreadySaved, !isModalPresentationPopOver {
            view().addButton.isEnabled = false
            navigationItem.title = view().saveString
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let navigationBar = navigationController?.navigationBar else { return }
        view().setAppearance(to: navigationBar)
    }
    
    // MARK: - Methods
    
    private func view() -> PresentView {
        let presentView = view as? PresentView
        return presentView ?? PresentView()
    }
    
    private func createBarButtons() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(donePush))
        navigationItem.rightBarButtonItem?.tintColor = R.color.appColors.buttonColor()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: R.image.systemIcons.share(), style: .done, target: self, action: #selector(shareNews))
        navigationItem.leftBarButtonItem?.tintColor = R.color.appColors.buttonColor()
    }
   
    // MARK: - Actions
    
    @IBAction func shareNews() {
        guard let url = news?.url else { return }
        let VC = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        present(VC, animated: true)
    }
    
    @IBAction func floatingButtonPush(_ sender: Any) {
        guard var navigationBarHeight: CGFloat = navigationController?.navigationBar.frame.size.height else { return }
        if !isModalPresentationPopOver {
            navigationBarHeight *= 2
        }
        view().scrollView.setContentOffset(CGPoint(x: 0, y: -navigationBarHeight), animated: true)
        view().floatingButton.isHidden = true
    }
    
    @IBAction func donePush() {
        dismiss(animated: true)
    }

    @IBAction func urlContentButtonPush() {
        if let news = news, let newsURL = URL(string: news.url),
           UIApplication.shared.canOpenURL(newsURL) {
            UIApplication.shared.open(newsURL, options: [:])
        }
    }
    
    @IBAction func addPush(_ sender: UIBarButtonItem) {
        let alertTitle = "Save not working"
        let alertMessage = "To use all capabilities of the app you should registrate \n"
        if !user.isLogged {
            showAlert(title: alertTitle, message: alertMessage, style: .alert)
        } else {
            if session.savedNews.isEmpty {
                NotificationCenter.default.post(name: .allImagesDownloaded, object: self)
            }
            
            observer.savedNewsAmount += 1

            view().createSaveView().fadeView { [weak self] in
                self?.navigationItem.title = self?.view().saveString
            }
            user.userInfo?.lastChangesDate = CommonModel.getCurrentDate()
            guard let news = news else { return }
            session.savedNews.append(news)
            view().addButton.isEnabled = false
        }
    }
}

extension PresentNewsViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < Constants.contentOffsetY {
            lastContentOffset = 0
            view().floatingButton.isHidden = true
            return
        }
        if lastContentOffset < scrollView.contentOffset.y {
            lastContentOffset = scrollView.contentOffset.y
            view().floatingButton.isHidden = false
        }
    }
}
