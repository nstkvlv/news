import UIKit

final class NewsModel {
    
    // MARK: - Properties
    
    private let session = AppDelegate.shared.session
    private let coreData = CoreDataHelper()
    private let user = UserDataService.shared
    private var requestInfo = (counter: 4, page: 1)
    private var swipedActionsAmount = (deleted: 0, shared: 0)
    
    let reachability = ReachabilityManager()
    let observer = Observer.shared
    let notification = NotificationCenter.default
    var tableConditions = (isEnd: false, isNeedToUpdate: false)
    var newsArray = [NewsStruct]()
    var archivedNews = [NewsStruct]()

    // MARK: - Methods
    
    func addInfo() {
         if user.isLogged {
            DispatchQueue.global(qos: .userInteractive).async {
                self.fetchSavedNews()
            }
        }
    }

    class func getNewsImage(for array: inout [NewsStruct]) {
        for i in 0..<array.count {
            array[i].image = NetworkingHelper().getImage(from: array[i].imageUrl ?? "")
        }
        NotificationCenter.default.post(name: .allImagesDownloaded, object: self)
    }
    
    private func fetchSavedNews() {
        guard let array = coreData.getSavedNews() else { return }
        session.savedNews = array
        NewsModel.getNewsImage(for: &session.savedNews)
    }
    
    func infiniteScroll() {
        if requestInfo.counter == 0 && tableConditions.isNeedToUpdate {
            requestInfo.page += 1
            getInfo()
            requestInfo.counter = 0
            tableConditions.isNeedToUpdate = false
            tableConditions.isEnd = false
        }
    }
    
    func refreshInfo() {
        requestInfo.counter = 4
        requestInfo.page = 1
        getInfo()
    }
    
    func getInfo() {
        NetworkingHelper().getNews(from: requestInfo.page) { [weak self] result in
            guard let self = self else { return }
            if self.requestInfo.counter == 4 {
                self.newsArray.removeAll()
            }
            self.newsArray += result
            DispatchQueue.main.async {
                self.notification.post(name: .hideLoadingView, object: self)
            }
            if self.requestInfo.counter > 0 {
                self.requestInfo.counter -= 1
                self.requestInfo.page += 1
                self.getInfo()
            }
        }
    }
}
