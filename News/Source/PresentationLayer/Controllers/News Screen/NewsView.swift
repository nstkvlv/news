import UIKit

final class NewsView: UIView {
    
    // MARK: - Outlets

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingView: UIView!
    
    // MARK: - Properties

    private let archivedNewsView = UIView.loadFromNibNamed(nibNamed: R.nib.archivatedNewsView.name)
    let countLabel = UILabel()
    let navigationBarRightButton = UIButton(type: .custom)
    let blurView = CommonModel.addBlurView()
    
    // MARK: - UIView lifecycle methods

    override func awakeFromNib() {
        super.awakeFromNib()
        navigationBarRightButton.isHidden = true
    }

    // MARK: - Constants

    private enum Constants {
        static let navBarRightButtonSize: CGFloat = 30
        static let navigationBarRightTrailingAnchor: CGFloat = 22
        static let navigationBarRightButtonBottomAnchor: CGFloat = 10
        static let countLabelWidthHeight: CGFloat = 13
        static let countLabelAlpha: CGFloat = 0.8
        static var archivatedViewHeight: CGFloat {
            if UserDataService.shared.isLogged {
                return GlobalConstants.archivatedNewsViewHeight - 50
            }
            return GlobalConstants.archivatedNewsViewHeight
        }
    }
    
    // MARK: - Methods

    func deselectRow() {
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func deleteArchiveView() {
        guard let window = AppDelegate.shared.window,
              let archivedView = window.subviews.last as? ArchivatedNewsView,
              let blurEffect = subviews.last as? UIVisualEffectView
        else { return }
        archivedView.removeFromSuperview()
        blurEffect.removeFromSuperview()
    }
    
    private func addBlurEffect() {
        let blur = UIVisualEffectView(effect: UIBlurEffect(style: .prominent))
        blur.frame = self.frame
        blur.alpha = 0.2
        blur.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blur)
        blur.fadeView(toAlpha: 0.8)
    }
    
    private func setUpView(archivedView: ArchivatedNewsView, on window: UIWindow ) {
        archivedView.setNeedsDisplay()
        archivedView.collectionView.reloadData()
        archivedView.frame = CGRect(x: 0, y: -Constants.archivatedViewHeight,
                                    width: window.frame.width,
                                    height: Constants.archivatedViewHeight)
    }
    
    func showArchiveNews(with completion: (_ archivedView: ArchivatedNewsView) -> ()) {
        guard let window = AppDelegate.shared.window,
              let archivedView = archivedNewsView as? ArchivatedNewsView
        else { return }
        
        setUpView(archivedView: archivedView, on: window)
        completion(archivedView)
        window.addSubview(archivedView)
        addBlurEffect()
        
        UIView.animate(
            withDuration: 1.3,
            delay: 0,
            options: .curveEaseIn,
            animations: {
                archivedView.frame = CGRect(x: 0, y: 0,
                                            width: archivedView.frame.width,
                                            height: archivedView.frame.height)
        })
    }
    
    func hideArchiveView() {
        guard let window = AppDelegate.shared.window,
              let blur = self.subviews.last as? UIVisualEffectView,
              let archivatedView = window.subviews.last as? ArchivatedNewsView
        else { return }

        blur.fadeView()
        UIView.animate(
            withDuration: 1,
            delay: 0,
            options: .curveEaseOut,
            animations: {
                archivatedView.frame = CGRect(x: 0, y: -archivatedView.frame.height,
                                              width: archivatedView.frame.width,
                                              height: archivatedView.frame.height)
        }, completion: { _ in
            archivatedView.removeFromSuperview()
            blur.removeFromSuperview()
        })
    }
    
    func setUp(navigationBar: UINavigationBar, newsAmount: Int) {
        navigationBar.prefersLargeTitles = true

        navigationBarRightButton.setImage(R.image.appImages.archiveButton(), for: .normal)
        navigationBar.addSubview(navigationBarRightButton)
        navigationBarButtonConstraints(on: navigationBar)
        
        createCountLabel(with: String(newsAmount))
        navigationBarRightButton.addSubview(countLabel)
        countLabelConstraints()
    }
    
    private func createCountLabel(with text: String) {
        countLabel.backgroundColor = R.color.appColors.buttonColor()
        countLabel.alpha = Constants.countLabelAlpha
        countLabel.layer.masksToBounds = true
        countLabel.layer.cornerRadius = Constants.countLabelWidthHeight / 2
        countLabel.font = .systemFont(ofSize: 10, weight: .regular)
        countLabel.text = text
        countLabel.textAlignment = .center
    }
    
    private func countLabelConstraints() {
        countLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            navigationBarRightButton.bottomAnchor.constraint(equalTo: countLabel.bottomAnchor),
            navigationBarRightButton.leadingAnchor.constraint(equalTo: countLabel.leadingAnchor, constant: 0),
            countLabel.widthAnchor.constraint(equalToConstant: Constants.countLabelWidthHeight),
            countLabel.heightAnchor.constraint(equalToConstant: Constants.countLabelWidthHeight)
        ])
    }
    
    private func navigationBarButtonConstraints(on navigationBar: UINavigationBar) {
        navigationBarRightButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            navigationBar.bottomAnchor.constraint(equalTo: navigationBarRightButton.bottomAnchor,
                                                  constant: Constants.navigationBarRightButtonBottomAnchor),
            navigationBar.trailingAnchor.constraint(equalTo: navigationBarRightButton.trailingAnchor,
                                                    constant: Constants.navigationBarRightTrailingAnchor),
            navigationBarRightButton.widthAnchor.constraint(equalToConstant: Constants.navBarRightButtonSize),
            navigationBarRightButton.heightAnchor.constraint(equalToConstant: Constants.navBarRightButtonSize)
        ])
    }
}
