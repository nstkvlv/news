import UIKit

protocol ArchivedNewsDelegate: class {
    func changeArray(newArray: [NewsStruct])
    func removeView()
}

final class ArchivatedNewsView: UIView {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var verticalSpacingCollectionViewCloseLine: NSLayoutConstraint!
    @IBOutlet private weak var registerTextView: UITextView!
    @IBOutlet private weak var warningLabel: UILabel!

    @IBOutlet weak var collectionView: UICollectionView!

    // MARK: - Properties

    private let user = UserDataService.shared
    private let coreData = CoreDataHelper()
    private let boldString = "After exiting the application, all archived news will disappear!"
    private let registerValue = "Register"
    private let longPress = UILongPressGestureRecognizer()
    private let commonModel = CommonModel()
    
    var archivedNewsArray = [NewsStruct]()
    var delegate: ArchivedNewsDelegate?
    
    // MARK: - Constants
    
    private enum Constants {
        static let cellWidthHeightDifference:CGFloat = 20
        static let verticalSpacingUnLoged: CGFloat = 68
        static let verticalSpacingLoged: CGFloat = 20
        static let fontsize: CGFloat = 14
        static let pressDuration = 0.5
        static let messageFontSize: CGFloat = 16
    }

    // MARK: - View methods
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        addGestureRecognizer()
        addLongPress()
        warningLabel.attributedText = warningLabel.text?.withBoldText(
            text: boldString,
            font:  .systemFont(ofSize: Constants.fontsize, weight: .thin),
            boldTextFont: .systemFont(ofSize: Constants.fontsize + 1, weight: .regular)
        )
        if user.isLogged {
            hideRegisterTextView()
        } else {
            addRegisterLink()
        }
    }
    
    // MARK: - Methods
    
    private func addLongPress() {
        longPress.addTarget(self, action: #selector(longPressToDelete))
        longPress.minimumPressDuration = Constants.pressDuration
        collectionView.addGestureRecognizer(longPress)
    }
    
    private func addRegisterLink() {
        verticalSpacingCollectionViewCloseLine.constant = Constants.verticalSpacingUnLoged
        registerTextView.isHidden = false
        let attributesForText: AttributesString = [
            .font: UIFont.systemFont(ofSize: Constants.fontsize, weight: .thin),
            .foregroundColor: UIColor.white]
        let attributedText = NSMutableAttributedString(string: registerTextView.text, attributes: attributesForText)

        let range = NSString(string: registerTextView.text).range(of: "REGISTER")
        let registerAttributes: AttributesString = [
            .link: registerValue,
            .font: UIFont.boldSystemFont(ofSize: Constants.fontsize + 2),
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        attributedText.addAttributes(registerAttributes, range: range)
        registerTextView.attributedText = attributedText
        registerTextView.textAlignment = .center
        guard let registerColor = R.color.appColors.buttonColor() else { return }
        registerTextView.linkTextAttributes = [.foregroundColor: registerColor]
    }
    
    private func hideRegisterTextView() {
        registerTextView.isHidden = true
        verticalSpacingCollectionViewCloseLine.constant = Constants.verticalSpacingLoged
    }
    
    private func animationToRemove(cell: ArchivatedNewsCell, at indexPath: IndexPath) {
        UIView.transition(
            with: cell,
            duration: 0.8,
            options: .transitionFlipFromRight,
            animations: {
                cell.newsImage.image = UIImage()
                cell.newsTitle.text = nil
                
                self.archivedNewsArray.remove(at: indexPath.item)
                self.collectionView.deleteItems(at: [indexPath])
            })
    }
    
    private func addGestureRecognizer() {
        let swipeUp = UISwipeGestureRecognizer(target: self,
                                               action: #selector(dismiss))
        swipeUp.direction = .up
        self.addGestureRecognizer(swipeUp)
    }
    
    // MARK: - Actions
    
    @IBAction func dismiss() {
        delegate?.changeArray(newArray: archivedNewsArray)
        delegate?.removeView()
    }
    
    @IBAction func longPressToDelete(longPress: UILongPressGestureRecognizer) {
        let point = longPress.location(in: collectionView)
        let index = collectionView.indexPathForItem(at: point)
        
        guard longPress.state == .began,
              let indexPath = index,
              let cell = collectionView.cellForItem(at: indexPath) as? ArchivatedNewsCell,
              let window = AppDelegate.shared.window else { return }
        
        cell.shakeIcon()
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        let deleteString = "Edit"
        alert.setValue(deleteString.withBoldText(text: deleteString), forKey: "attributedTitle")

        let messageAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: Constants.messageFontSize)]
        let messageString = NSAttributedString(string: "This actions can't be canceled", attributes: messageAttributes)
        alert.setValue(messageString, forKey: "attributedMessage")
        
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: { [weak self] _ in
            guard let self = self else { return }
            self.animationToRemove(cell: cell, at: indexPath)
        })
        
        if user.isLogged {
            let saveAction = UIAlertAction(title: "Save", style: .default) { [weak self] _ in
                guard let self = self else { return }
//                observer.savedNewsAmount += 1
                let news = self.archivedNewsArray[indexPath.item]
                self.user.userInfo?.lastChangesDate = CommonModel.getCurrentDate()
                AppDelegate.shared.session.savedNews.append(news)
                self.animationToRemove(cell: cell, at: indexPath)

            }
            saveAction.setValue(R.color.appColors.buttonColor(), forKey: "titleTextColor")
            alert.addAction(saveAction)
        }
            
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            cell.stopShakingIcon()
        })
        
        cancelAction.setValue(R.color.appColors.buttonColor(), forKey: "titleTextColor")
        
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        window.rootViewController?.present(alert, animated: true)
    }
}

extension ArchivatedNewsView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return archivedNewsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(R.nib.archivatedNewsCell)
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.archivatedNewsCell, for: indexPath)
            else { return UICollectionViewCell() }
        cell.newsImage.roundImage(cornerRadius: 15)
        cell.newsImage.image = archivedNewsArray[indexPath.item].image
        cell.newsTitle.text = archivedNewsArray[indexPath.item].title
        
        return cell
    }
}

extension ArchivatedNewsView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let blur = UIView.loadFromNibNamed(nibNamed: R.nib.blurView.name)
        guard let window = AppDelegate.shared.window,
              let VC = R.storyboard.newsInfo.presentNewsViewController(),
              let blurView = blur else { return }
        
        blurView.isHidden = false
        window.addSubview(blurView)
        
        VC.news = archivedNewsArray[indexPath.item]
        VC.navigationController?.navigationBar.isHidden = true
        VC.modalPresentationStyle = .popover
        let navigationVC = UINavigationController(rootViewController: VC)
        navigationVC.modalPresentationStyle = .popover
        window.rootViewController?.present(navigationVC, animated: true,  completion: {
            blurView.removeFromSuperview()
        })
    }
}

extension ArchivatedNewsView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2 - Constants.cellWidthHeightDifference, height: collectionView.frame.height - Constants.cellWidthHeightDifference)
    }
}

extension ArchivatedNewsView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        guard URL.absoluteString == registerValue,
              let VC = R.storyboard.auth.logIn() else { return false }
        AppDelegate.shared.makeRootController(controller: VC)
        return false
    }
}
