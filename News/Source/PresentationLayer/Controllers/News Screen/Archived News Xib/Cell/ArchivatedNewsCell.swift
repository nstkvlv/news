import UIKit

class ArchivatedNewsCell: UICollectionViewCell {
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    
    func shakeIcon() {
          let shakeAnimation = CABasicAnimation(keyPath: "transform.rotation")
          let startAngle: Float = (-1) * 3.14159/180
          let stopAngle = -startAngle
          shakeAnimation.fromValue = NSNumber(value: startAngle as Float)
          shakeAnimation.toValue = NSNumber(value: 3 * stopAngle as Float)
          shakeAnimation.autoreverses = true
          shakeAnimation.duration = 0.3
          shakeAnimation.repeatCount = 10000
          shakeAnimation.timeOffset = 290 * drand48()
          
          let layer: CALayer = self.layer
          layer.add(shakeAnimation, forKey:"shaking")
      }
      
      func stopShakingIcon() {
          let layer: CALayer = self.layer
          layer.removeAnimation(forKey: "shaking")
      }
}

