import UIKit

final class NewsViewController: UIViewController {
    
    // MARK: - Properties
    
    private let refreshTable = UIRefreshControl()
    private let model = NewsModel()
    
    // MARK: - View controller lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model.reachability.delegate = self
        tabBarController?.tabBar.tintColor = R.color.appColors.buttonColor()
        model.addInfo()
        setNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavigationBar()
        pullToRefresh()
        view().deselectRow()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view().navigationBarRightButton.removeFromSuperview()
        view().deleteArchiveView()
    }
    
    // MARK: - Methods
    
    func view() -> NewsView {
        let newsView = view as? NewsView
        return newsView ?? NewsView()
    }
    
    private func setNotifications() {
        model.notification.addObserver(forName: .clearArchivatedArray, object: nil, queue: nil) { [weak self] _ in
            guard let self = self else { return }
            self.model.archivedNews.removeAll()
            self.view().navigationBarRightButton.isHidden = true
        }
        
        model.notification.addObserver(forName: .hideLoadingView, object: nil, queue: nil) { [weak self] _ in
            guard let self = self else { return }
            self.view().tableView.reloadData()
            if let loadingView = self.view().loadingView {
                loadingView.removeFromSuperview()
            }
        }
    }
    
    private func customNavigationBar() {
        guard let navigationBar = navigationController?.navigationBar else { return }
        view().setUp(navigationBar: navigationBar,
                     newsAmount: model.archivedNews.count)
        view().navigationBarRightButton.addTarget(self, action: #selector(showArchivedNews), for: .touchUpInside)
    }
    
    private func pullToRefresh() {
        refreshTable.tintColor = R.color.appColors.buttonColor()
        refreshTable.addTarget(self, action: #selector(refresh), for: .valueChanged)
        view().tableView.addSubview(refreshTable)
    }
    
    // MARK: - Actions
    
    @IBAction func refresh() {
        model.refreshInfo()
        DispatchQueue.main.async {
            self.refreshTable.endRefreshing()
        }
    }
    
    @IBAction func showArchivedNews() {
        view().showArchiveNews { archivedView in
            archivedView.archivedNewsArray = model.archivedNews.reversed()
            archivedView.delegate = self
        }
    }
}

extension NewsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GlobalConstants.newsCellHeight
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        view().blurView.isHidden = false
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        view().blurView.isHidden = true
    }
        
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let archiveAction = UIContextualAction(style: .normal, title: "Archivate", handler: { [weak self] action, view, completionHandler in
            guard let self = self else { return }
            self.model.archivedNews.append(self.model.newsArray[indexPath.row])
            self.model.newsArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .left)
            self.view().navigationBarRightButton.isHidden = false
            self.view().countLabel.text = String(self.model.archivedNews.count)
            completionHandler(true)
        })
        archiveAction.image = R.image.systemIcons.archiveBox()
        archiveAction.backgroundColor = R.color.appColors.buttonColor()

        return UISwipeActionsConfiguration(actions: [archiveAction])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let shareAction = UIContextualAction(style: .normal, title: "Share", handler: { [weak self] action, view, completionHandler in
            guard let self = self else { return }
            let url = self.model.newsArray[indexPath.row].url
            let VC = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            self.present(VC, animated: true)
            VC.completionWithItemsHandler = { [weak self] activityType, completed, returnedItems, error in
                guard let self = self, completed else { return }
                self.model.observer.sharedNewsAmount += 1
            }
            completionHandler(true)
        })
        shareAction.image = R.image.systemIcons.share()
        shareAction.backgroundColor = .systemOrange
                
        return UISwipeActionsConfiguration(actions: [shareAction])
    }
}

extension NewsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.newsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        model.infiniteScroll()
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.newsCell, for: indexPath)
            else { return UITableViewCell() }
        cell.authorLabel.text = CommonModel.getAuthor(for: model.newsArray[indexPath.row])
        cell.titleLabel.text = model.newsArray[indexPath.row].title
        cell.newsImage.roundImage(cornerRadius: 8)
        cell.newsImage.image = model.newsArray[indexPath.row].image
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let VC = R.storyboard.newsInfo.presentNewsViewController() {
            VC.news = model.newsArray[indexPath.row]
            navigationController?.pushViewController(VC, animated: true)
        }
        view().blurView.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let lastIndexPath = tableView.indexPathsForVisibleRows?.last else { return }
        if lastIndexPath.row <= indexPath.row {
            cell.center.y = cell.center.y + cell.frame.height / 2
            cell.alpha = 0
            UIView.animate(withDuration: 0.5,
                           delay: 0.2,
                           options: .curveEaseInOut,
                           animations: {
                            cell.center.y = cell.center.y - cell.frame.height / 2
                            cell.alpha = 1
            })
        }
    }
}

extension NewsViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentOffsetY = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentOffsetY
        if distanceFromBottom < height && !model.tableConditions.isEnd {
            model.tableConditions.isNeedToUpdate = true
            model.tableConditions.isEnd = true
        }
    }
}

extension NewsViewController: ArchivedNewsDelegate {
    func changeArray(newArray: [NewsStruct]) {
        model.archivedNews = newArray
        view().countLabel.text = String(model.archivedNews.count)
    }
    
    func removeView() {
        view().hideArchiveView()
    }
}

extension NewsViewController: ConnectionDelegate {
    func connected(navigationTitle: String) {
        title = navigationTitle
        navigationController?.navigationBar.prefersLargeTitles = true
        model.refreshInfo()
    }
    
    func disconnected(navigationTitle: String) {
        title = navigationTitle
        navigationController?.navigationBar.prefersLargeTitles = false
    }
}
