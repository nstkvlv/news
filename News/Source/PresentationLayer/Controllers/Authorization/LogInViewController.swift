import UIKit

final class LogInViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var password: AuthTextField!
    @IBOutlet private weak var email: AuthTextField!
    @IBOutlet private weak var registrationButton: UIButton!
    @IBOutlet private weak var errorLabel: UILabel!
    
    // MARK: - Properties

    private let session = AppDelegate.shared.session
    
    // MARK: - View controller lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        styleRegisterButton()
    }

    // MARK: - Methods
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    private func styleRegisterButton() {
        let yourAttributes: AttributesString = [
            .font: UIFont.systemFont(ofSize: 18),
            .foregroundColor: R.color.appColors.mainColor() as Any,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributedString = NSMutableAttributedString(string: "Registration ", attributes: yourAttributes)
        registrationButton.setAttributedTitle(attributedString, for: .normal)
    }
    
    // MARK: - Actions
    
    @IBAction func pushLogIn() {
        guard let email = email.text, let password = password.text else { return }
        if email.isEmpty || password.isEmpty {
            errorLabel.isHidden = false
            errorLabel.text = "Empty Fields"
            return
        }
        CoreDataHelper().logIn(email: email, password: password) { [weak self] error, array in
            guard let self = self else { return }
            if let array = array {
                self.session.savedNews = array
                NewsModel.getNewsImage(for: &self.session.savedNews)
            }
            if let error = error {
                self.errorLabel.isHidden = false
                self.errorLabel.text = error
            } else {
                UserDataService.shared.isLogged = true
                guard let VC = R.storyboard.main.mainTabBarController() else { return }
                AppDelegate.shared.makeRootController(controller: VC)
            }
        }
    }
    
    @IBAction func cancelPush(_ sender: UIBarButtonItem) {
        guard let VC = R.storyboard.main.mainTabBarController() else { return }
        AppDelegate.shared.makeRootController(controller: VC)
    }
}

extension LogInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
            case email:
                password.becomeFirstResponder()
            default:
                textField.resignFirstResponder()
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        errorLabel.isHidden = true
    }
}
