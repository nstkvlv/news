import UIKit

final class BlockScreenViewController: UIViewController {
    
    // MARK: - Outlets

    @IBOutlet private weak var unlockLabel: UILabel!
    
    // MARK: - Properties

    private let biometricAuthentication = BiometricAuthentication()
    
    // MARK: - View controller lifecycle methods

    override func viewDidLoad() {
        unlockLabel.text = "To unlock the app use \(biometricAuthentication.biometricType())"
    }
    
    // MARK: - Methods

    @IBAction func unlockButtonPush(_ sender: UIButton) {
        biometricAuthentication.authenticate(
            onSuccess: {
                sender.imageView?.image = R.image.appImages.unlock()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.dismiss(animated: true)
                }
        })
    }
}
