import UIKit
import LocalAuthentication

final class BiometricAuthentication {
    
    // MARK: - Properties

    private let faceIdLockedErrorCode = -8
    private var localAuthenticationContext = LAContext()
    private var authorizationError: NSError?
    var isFaceIDAvailiable: Bool {
        return localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
                                                            error: &authorizationError)
    }
    
    // MARK: - Methods
    
    func authenticate(onSuccess: @escaping() -> (), onCancel: @escaping() -> () = { }) {
        if isFaceIDAvailableInApp() {
            let reason = "Use Face ID for an additional security measure"
            localAuthenticationContext.localizedFallbackTitle = "Please use your Passcode"
            evaluatePolicy(reason: reason, onSuccess: onSuccess, onCancel: onCancel)
        } else {
            guard let error = authorizationError?.localizedDescription else { return }
            print(error)
            
            if authorizationError?.code == faceIdLockedErrorCode {
                let reason = error
                evaluatePolicy(reason: reason, onSuccess: onSuccess, onCancel: onCancel)
            }
        }
    }
    
    private func evaluatePolicy(reason: String, onSuccess: @escaping() -> (), onCancel: @escaping() -> () = { }) {
        localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) { success, evaluateError in
            DispatchQueue.main.async {
               if success {
                   onSuccess()
               } else {
                   guard let error = evaluateError?.localizedDescription else { return }
                   print(error)
                   onCancel()
               }
           }
        }
    }
    
    func isFaceIDAvailableInApp() -> Bool {
        if !isFaceIDAvailiable {
            return false
        }
        
        if let isFaceIdAvailable = UserDataService.shared.settings?.isFaceIdAvailable,
           isFaceIdAvailable {
            return true
        }
        
        return false
    }
    
    func biometricType() -> String {
        switch localAuthenticationContext.biometryType  {
            case .faceID:
                return "Face ID"
            case .touchID:
                return "Touch ID"
            case .none:
                return "Passcode"
            default:
                return "Unknown"
        }
    }
    
    func showBlockScreen() {
        if let VC = R.storyboard.auth.blockScreenViewController(),
           let window = AppDelegate.shared.window {
            VC.modalPresentationStyle = .fullScreen
            window.rootViewController?.present(VC, animated: true)
        }
    }
}
