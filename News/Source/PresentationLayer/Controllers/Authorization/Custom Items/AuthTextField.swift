import UIKit

@IBDesignable
final class AuthTextField: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        textFieldSetup()
        setBottomBorder()
    }
    
    private func textFieldSetup() {
        layer.masksToBounds = true
        font = font?.withSize(28)
    }
}
