import UIKit

@IBDesignable
final class CustomButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        layer.cornerRadius = 20
        layer.backgroundColor = R.color.appColors.mainColor()?.cgColor
        setTitleColor(.black, for: .normal)
    }
}
