import UIKit

final class RegisterViewController: UIViewController {
    
    // MARK: - Outlets

    @IBOutlet private weak var repeatPassword: AuthTextField!
    @IBOutlet private weak var password: AuthTextField!
    @IBOutlet private weak var email: AuthTextField!
    @IBOutlet private weak var name: AuthTextField!
    @IBOutlet private weak var errorLabel: UILabel!
    
    // MARK: - Properties

    private let user = UserDataService.shared
    
    // MARK: - Methods

    private func showError(text: String) {
        errorLabel.isHidden = false
        errorLabel.text = text
    }
    
    // MARK: - Actions
    
    @IBAction func submitPush(_ sender: UIButton) {
        guard let name = name.text,
              let email = email.text,
              let password = password.text,
              let repeatPassword = repeatPassword.text else { return }
        
        if name.isEmpty || email.isEmpty || password.isEmpty || repeatPassword.isEmpty {
            showError(text: "Empty fields")
            return
        }
        if password.count < 5 {
            showError(text: "Password must contain more than 5 symbols")
            return
        }
        if password == repeatPassword {
            CoreDataHelper().registration(email: email, password: password, name: name) { [weak self] in
                self?.user.isLogged = true
                guard let VC = R.storyboard.main.mainTabBarController() else { return }
                AppDelegate.shared.makeRootController(controller: VC)
            }
        } else {
            showError(text: "Different passwords")
        }
    }
}

extension RegisterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
            case name:
                email.becomeFirstResponder()
            case email:
                password.becomeFirstResponder()
            case password:
                repeatPassword.becomeFirstResponder()
            default:
                textField.resignFirstResponder()
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        errorLabel.isHidden = true
    }
}
