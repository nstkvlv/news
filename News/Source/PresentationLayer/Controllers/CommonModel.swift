import UIKit

final class CommonModel {
    
    class func getAuthor(for news: NewsStruct) -> String {
        let authorsAmount = 3
        return news.author.prefix(authorsAmount).joined(separator: ", ")
    }
    
    class func addBlurView() -> UIView {
        guard let blurView = UIView.loadFromNibNamed(nibNamed: R.nib.blurView.name),
              let window = AppDelegate.shared.window
        else { return UIView() }
        blurView.frame = CGRect(x: 0, y: 0,
                                width: window.frame.width,
                                height: window.frame.height)
        window.addSubview(blurView)
        blurView.isHidden = true
        return blurView
    }
    
    class func getCurrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM"
        let currentDate = formatter.string(from: date)
        return currentDate
    }
}
