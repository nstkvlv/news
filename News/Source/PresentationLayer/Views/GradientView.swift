import UIKit

@IBDesignable
class GradientView: UIView {
    @IBInspectable var startColor: UIColor = .clear {
        didSet { setNeedsLayout() }
    }
    @IBInspectable var endColor: UIColor = .clear {
        didSet { setNeedsLayout() }
    }
    
    @IBInspectable var shadowColor: UIColor = .clear {
        didSet { setNeedsLayout() }
    }
    
    @IBInspectable var shadowX: CGFloat = 0 {
        didSet { setNeedsLayout() }
    }

    @IBInspectable var shadowY: CGFloat = -3 {
        didSet { setNeedsLayout() }
    }

    @IBInspectable var shadowBlur: CGFloat = 3 {
        didSet { setNeedsLayout() }
    }

    @IBInspectable var startPointX: CGFloat = 0 {
        didSet { setNeedsLayout() }
    }

    @IBInspectable var startPointY: CGFloat = 0 {
        didSet { setNeedsLayout() }
    }

    @IBInspectable var endPointX: CGFloat = 0 {
        didSet { setNeedsLayout() }
    }

    @IBInspectable var endPointY: CGFloat = 0 {
        didSet { setNeedsLayout() }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet { setNeedsLayout() }
    }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        guard let gradientLayer = layer as? CAGradientLayer else { return }
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: startPointX, y: startPointY)
        gradientLayer.endPoint = CGPoint(x: endPointX, y: endPointY)
        layer.cornerRadius = cornerRadius
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = CGSize(width: shadowX, height: shadowY)
        layer.shadowRadius = shadowBlur
        layer.shadowOpacity = 1
    }
}
