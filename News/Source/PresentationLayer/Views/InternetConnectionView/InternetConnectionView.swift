import UIKit

final class InternetConnectionView: UIView {
    
    @IBOutlet private weak var connectionLabel: UILabel!
    @IBOutlet private weak var connectionImage: UIImageView!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpBackground()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.masksToBounds = true
        layer.cornerRadius = 25
    }
    
    func setUpBackground() {
        if traitCollection.userInterfaceStyle == .light {
            backgroundColor = .lightGray
        }
    }
}

